import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import NotFoundPage from "./pages/NotFoundPage";
import HomePage from "./pages/HomePage";
import RegisterPage from "./pages/RegisterPage";
import { Provider } from "react-redux";
import { store } from "./store";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import frJson from './i18n/fr.json';
import enJson from './i18n/en.json';
import List from "./pages/List";
import { ListBoat, ListRescued, ListRescuer } from "./api/list";
import RescuerDetail from "./components/ItemDetail/RescuerDetail";
import RescuedDetail from "./components/ItemDetail/RescuedDetail";
import BoatDetail from "./components/ItemDetail/BoatDetail";
import { QueryClient, QueryClientProvider } from "react-query";

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    lng: "fr",
    resources: {
      fr: {
        translation: frJson
      },
      en: {
        translation: enJson
      }
    },
    interpolation: {
      escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
    }
  });

const columnPerson: string[] = [
  "PRENOM",
  "NOM",
  "DATE_DE_NAISSANCE",
  "DATE_DE_DECES"
];
const columnBoat: string[] = [
  "NOM"
];

const queryClient:any = new QueryClient();

ReactDOM.render(
  <React.StrictMode>
   <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
            <Route path="/list/rescuer" element={<List title={'SAUVETEURS'} detailComponent={RescuerDetail} column={columnPerson} getList={ListRescuer}/>} />
            <Route path="/list/rescued" element={<List title={'SORTIE_EN_MER'} detailComponent={RescuedDetail} column={columnPerson} getList={ListRescued}/>} />
            <Route path="/list/boat" element={<List title={'BATEAUX'} detailComponent={BoatDetail} column={columnBoat} getList={ListBoat}/>} />
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    </QueryClientProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
