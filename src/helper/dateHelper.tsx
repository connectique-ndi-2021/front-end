export const isDateTransformString = (str: string) => {
    if(/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$/.test(str)){
        return new Date(str).toLocaleDateString();
    }
    return str;
}