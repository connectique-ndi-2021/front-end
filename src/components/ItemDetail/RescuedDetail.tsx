import { ExclamationIcon } from "@heroicons/react/outline";
import { useTranslation } from "react-i18next";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { GetShipwrecksForSavedPeople } from "../../api/list";
import { isDateTransformString } from "../../helper/dateHelper";

export default function RescuedDetail(props: any) {
  const { t } = useTranslation();
  const { data, status } = useQuery<any>([], () => GetShipwrecksForSavedPeople(props.item.id));

  return (<>
    <h2 className="text-2xl font-extrabold text-gray-900 sm:pr-12">{props.item.name} {props.item.surname}</h2>
    <section aria-labelledby="information-heading" className="mt-3">
      <div className="mt-6 flex gap-2">
        <p className="text-sm text-gray-700 font-bold">{t('PRENOM')} :</p>
        <p className="text-sm text-gray-700">{props.item.name}</p>
      </div>

      <div className="mt-6 flex gap-2">
        <p className="text-sm text-gray-700 font-bold">{t('NOM')} :</p>
        <p className="text-sm text-gray-700">{props.item.surname}</p>
      </div>

      <div className="mt-6 flex gap-2">
        <p className="text-sm text-gray-700 font-bold">{t('DATE_DE_NAISSANCE')} :</p>
        <p className="text-sm text-gray-700">{isDateTransformString(props.item.birthDate)}</p>
      </div>

      <div className="mt-6 flex gap-2">
        <p className="text-sm text-gray-700 font-bold">{t('DATE_DE_DECES')} :</p>
        <p className="text-sm text-gray-700">{isDateTransformString(props.item.deathDate)}</p>
      </div>

      <div className="mt-6 flex gap-2">
        <p className="text-sm text-gray-700 font-bold">{t('SAUVETAGES')} :</p>
      </div>

      <div className="bg-white shadow overflow-hidden sm:rounded-md mt-5">
        <ul className="divide-y divide-gray-200">
          {status === "success" &&
            data.length > 0
            ? data.map((detail: any) => (
              <li key={detail.id}>
                <Link to="#" className="block hover:bg-gray-50">
                  <div className="px-4 py-4 sm:px-6">
                    <div className="flex items-center justify-between">
                      <p className="text-sm font-medium text-indigo-600 truncate">{isDateTransformString(detail.date)}</p>
                      <p className="text-sm text-gray-900">
                        {detail.description}
                      </p>
                    </div>
                  </div>
                </Link>
              </li>
            ))
            : (<div className="bg-yellow-50 border-l-4 border-yellow-400 p-4">
              <div className="flex">
                <div className="flex-shrink-0">
                  <ExclamationIcon className="h-5 w-5 text-yellow-400" aria-hidden="true" />
                </div>
                <div className="ml-3">
                  <p className="text-sm text-yellow-700">
                    {t('PAS_DE_SAUVETAGE_LIE')}
                  </p>
                </div>
              </div>
            </div>
            )}
        </ul>
      </div>
    </section>
  </>);
}