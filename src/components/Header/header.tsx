import { useTranslation } from "react-i18next";
import logo from '../../assets/images/logo.svg';
import { Fragment, useState } from 'react'
import { Menu, Popover, Transition } from '@headlessui/react'
import { ChevronDownIcon, MenuIcon, TrashIcon, XIcon } from '@heroicons/react/outline'
import { Link } from "react-router-dom";
import i18next from "i18next";
function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ')
}

export default function Header() {
  const { t } = useTranslation();

  const navigation = [
    { name: t('SORTIE_EN_MER'), href: '/list/rescued' },
    { name: t('SAUVETEURS'), href: '/list/rescuer' },
    { name: t('BATEAUX'), href: '/list/boat' },
  ]

  const [color, setColor] = useState<[number, number, number]>([79, 70, 229]);
  const [nClick, setNClick] = useState<number>(0);

  const onLogoDoubleClick = () => {
    setColor([
      Math.random() * 255,
      Math.random() * 255,
      Math.random() * 255,
    ]);

    setNClick(nClick + 1);
  }

  return (<Popover>
    <nav
      className="relative max-w-7xl mx-auto flex items-center justify-between px-4 sm:px-6"
      aria-label="Global"
    >
      <div className="flex items-center flex-1">
        <div className="flex items-center justify-between w-full md:w-auto">
          <div className="flex items-center">
            <Link to="/" onDoubleClick={onLogoDoubleClick}>
              <svg xmlns="http://www.w3.org/2000/svg" width="556" height="81" viewBox="0 0 556 81" className="h-8 w-auto sm:h-10">
                <path id="Connect_ique" data-name="Connect&apos;ique" style={{ fill: `rgb(${color[0]}, ${color[1]} ,${color[2]})`, fillRule: "evenodd" }} d="M56.1,56q-6.5.4-10.85,0.7t-7.1.4l-5.45.2q-2.7.1-5.2,0.15t-4.6.05a4.672,4.672,0,0,1-3.45-1.35A4.93,4.93,0,0,1,18.1,52.5v-32q0-6,6-6h32V0.5h-36Q9.6,0.5,4.855,5.25T0.1,20.5v32q0,9.8,4.6,14.4t14.4,4.6q14.7,0,37-2V56Zm61-23.5q0-7.5-4.25-11.75T101.1,16.5h-23q-7.5,0-11.75,4.25T62.1,32.5v23q0,7.5,4.25,11.75T78.1,71.5h23q7.5,0,11.75-4.25T117.1,55.5v-23ZM99.6,54q0,4-4,4h-12q-4,0-4-4V34q0-4,4-4h12q4,0,4,4V54Zm59.5-23a3.418,3.418,0,0,1,3.5,3.5v36h17.5v-39q0-6.8-4.1-10.9t-10.9-4.1h-4.5a30.717,30.717,0,0,0-8.35,1.45,35.817,35.817,0,0,0-9.65,4.55l-1-5H126.1v53h17.5V33a42.486,42.486,0,0,1,12-2h3.5Zm63,0a3.418,3.418,0,0,1,3.5,3.5v36h17.5v-39q0-6.8-4.1-10.9t-10.9-4.1h-4.5a30.717,30.717,0,0,0-8.35,1.45,35.817,35.817,0,0,0-9.65,4.55l-1-5H189.1v53h17.5V33a42.486,42.486,0,0,1,12-2h3.5Zm59.5-2q4,0,4,4v5.5h-17V33q0-4,4-4h9Zm19.5,28q-19.1,1.5-29,1.5a3.418,3.418,0,0,1-3.5-3.5V51h34.5V32.5q0-7.5-4.25-11.75T287.1,16.5h-20q-7.5,0-11.75,4.25T251.1,32.5v24q0,6.8,4.1,10.9t10.9,4.1a342.469,342.469,0,0,0,35-2V57Zm58-.5q-18,1.5-27,1.5a3.418,3.418,0,0,1-3.5-3.5V35q0-4,4-4h26.5V17.5h-32q-7.5,0-11.75,4.25T311.1,33.5v23q0,6.8,4.1,10.9t10.9,4.1a314.769,314.769,0,0,0,33-2v-13Zm45,1h-13a3.418,3.418,0,0,1-3.5-3.5V31h12.5V17.5H387.6V5.5H372.1l-2,12h-6V31h6V56.5q0,6.8,4.1,10.9t10.9,4.1a185.552,185.552,0,0,0,19-1v-13Zm18-32,2-16v-9h-15v9l2,16h11Zm6.2,14.4V48h4.5V70.5h10.5V39.9h-15Zm4.5-3.6h10.5V27.9H432.8v8.4ZM461.3,63a2.051,2.051,0,0,1-2.1-2.1V49.2a2.122,2.122,0,0,1,2.4-2.4H470V62.4a41.6,41.6,0,0,1-7.2.6h-1.5ZM470,80.7h10.5v-42H458.3a8.832,8.832,0,0,0-9.6,9.6V62.1a8.462,8.462,0,0,0,9,9h2.1A51.378,51.378,0,0,0,470,69.9V80.7Zm29.1-18.3a2.051,2.051,0,0,1-2.1-2.1V38.7H486.5V62.1a8.462,8.462,0,0,0,9,9h2.7a18.436,18.436,0,0,0,5.01-.87A21.451,21.451,0,0,0,509,67.5l0.6,3h9.3V38.7H508.4V61.2a25.463,25.463,0,0,1-7.2,1.2h-2.1Zm43.5-16.8A2.122,2.122,0,0,1,545,48v3.3H534.8V48a2.122,2.122,0,0,1,2.4-2.4h5.4Zm11.7,16.8q-11.462.9-17.4,0.9a2.051,2.051,0,0,1-2.1-2.1V58.8h20.7V47.7a8.832,8.832,0,0,0-9.6-9.6h-12a8.832,8.832,0,0,0-9.6,9.6V62.1a8.462,8.462,0,0,0,9,9,205.482,205.482,0,0,0,21-1.2V62.4Z" />
              </svg>
            </Link>
            {nClick >= 3 && <a href="https://ndi2021-game.connectique.eu.org/"><TrashIcon className="h-6 w-6 ml-2 text-red-500" /></a>}
          </div>
          <div className="-mr-2 flex items-center md:hidden">
            <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
              <span className="sr-only">Open main menu</span>
              <MenuIcon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
          </div>
        </div>
        <div className="hidden md:block md:ml-10 md:space-x-10">
          {navigation.map((item) => (
            <Link key={item.name} to={item.href} className="font-medium text-gray-500 hover:text-gray-900">
              {item.name}
            </Link>
          ))}
        </div>
      </div>
      <div className="mr-2">
        <Menu as="div" className="relative inline-block text-left">
          <div>
            <Menu.Button className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500">
              Lang
              <ChevronDownIcon className="-mr-1 ml-2 h-5 w-5" aria-hidden="true" />
            </Menu.Button>
          </div>

          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
              <div className="py-1">
                <Menu.Item>
                  {({ active }) => (
                    <a
                      onClick={() => {i18next.changeLanguage("fr");}}
                      className={classNames(
                        active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                        'block px-4 py-2 text-sm'
                      )}>
                      Français
                    </a>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <a
                      onClick={() => {i18next.changeLanguage("en");}}
                      className={classNames(
                        active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                        'block px-4 py-2 text-sm'
                      )}>
                      English
                    </a>
                  )}
                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </Menu>
      </div>
      <div className="hidden md:block text-right">
        <span className="inline-flex rounded-md shadow-md ring-1 ring-black ring-opacity-5 mr-2">
          <Link
            to="/login"
            className="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700"
          >
            {t('LOGIN')}
          </Link>
        </span>
        <span className="inline-flex rounded-md shadow-md ring-1 ring-black ring-opacity-5">
          <Link
            to="/register"
            className="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md text-blue-600 bg-white hover:bg-gray-50"
          >
            {t('REGISTER')}
          </Link>
        </span>
      </div>
    </nav>

    <Transition
      as={Fragment}
      enter="duration-150 ease-out"
      enterFrom="opacity-0 scale-95"
      enterTo="opacity-100 scale-100"
      leave="duration-100 ease-in"
      leaveFrom="opacity-100 scale-100"
      leaveTo="opacity-0 scale-95"
    >
      <Popover.Panel
        focus
        className="absolute z-10 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
      >
        <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
          <div className="px-5 pt-4 flex items-center justify-between">
            <div>
              <img
                className="h-8 w-auto"
                src={logo}
                alt="Logo"
              />
            </div>
            <div className="-mr-2">
              <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                <span className="sr-only">Close main menu</span>
                <XIcon className="h-6 w-6" aria-hidden="true" />
              </Popover.Button>
            </div>
          </div>
          <div className="px-2 pt-2 pb-3 space-y-1">
            {navigation.map((item) => (
              <Link
                key={item.name}
                to={item.href}
                className="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50"
              >
                {item.name}
              </Link>
            ))}
          </div>

          <Link
            to="/login"
            className="block w-full px-5 py-3 text-center font-medium text-white bg-blue-600"
          >
            {t('LOGIN')}
          </Link>
          <Link
            to="/register"
            className="block w-full px-5 py-3 text-center font-medium text-blue-600 bg-gray-50 hover:bg-gray-100"
          >
            {t('REGISTER')}
          </Link>
        </div>
      </Popover.Panel>
    </Transition>
  </Popover>);
}