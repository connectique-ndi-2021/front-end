import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./style.module.css";

export interface LoadingProps {
  visible?: boolean;
  style?: React.CSSProperties | undefined;
}

export function Loading(props: LoadingProps) {
  if (props.visible === false) {
    return <></>;
  }

  return (
    <div className={styles.loaderContainer} style={props.style}>
      <FontAwesomeIcon icon={faSpinner} spin />
    </div>
  );
}
