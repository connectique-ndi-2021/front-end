import { Loading, LoadingProps } from ".";
import styles from "./style.module.css";

export function FullScreenLoading(props: LoadingProps) {
  if (props.visible === false) {
    return <></>;
  }

  return (
    <div className={styles.fullScreen}>
      <Loading {...props} />
    </div>
  );
}
