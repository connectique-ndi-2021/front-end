import { ChevronRightIcon, SearchIcon } from "@heroicons/react/outline";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useQuery } from "react-query";
import Header from "../components/Header/header";
import ModalDetail from "../components/ItemDetail/ModalDetail";
import { isDateTransformString } from "../helper/dateHelper";

interface ListProps {
    getList: any;
    column: string[];
    detailComponent: any;
    title: string;
}

export default function List(props: ListProps) {
    const { t } = useTranslation();
    const [show, setShow] = useState<boolean>(false);
    const { data, status } = useQuery<any>([props.title], props.getList);
    const [itemClicked, setItemClicked] = useState<any>({});
    const [searchValue, setSearchValue] = useState<string>("");

    const title = props.title;
    useEffect(() => {
        setSearchValue("");
    }, [title]);

    const handleItemClick = (item: any) => {
        setItemClicked(item);
        console.log(item);
        setShow(true);
    }

    const filterFunction = (items: any[]) => {
        let found = true;

        searchValue.split(" ").forEach(s => {
            let f = false;

            Object.values(items).forEach(item => {
                f = f || isDateTransformString(item).toString().toLowerCase().includes(s.toLowerCase());
            })

            found = found && f;
        })

        return found;
    }

    return (<div className="relative pt-6 pb-16 sm:pb-24 lg:pb-32">
        <Header />
        <ModalDetail childrenComponent={props.detailComponent} item={itemClicked} open={show} setOpen={setShow} />
        <div className="px-4 max-w-6xl mx-auto px-4 sm:px-6 lg:px-8 ">

            <form className="mt-8 sm:flex justify-between items-center">
                <h1 className="text-lg font-medium leading-6 text-gray-900 sm:truncate">
                    {t(props.title)}
                </h1>
                <div className="sm:flex">
                    <input
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        id="search-bar"
                        name="search"
                        type="text"
                        required
                        className="w-full px-5 py-3 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs border-gray-300 rounded-md"
                        placeholder={t('RECHERCHER')}
                    />
                    <div className="mt-3 rounded-md sm:mt-0 sm:ml-3 sm:flex-shrink-0">
                        <button
                            type="submit"
                            className="w-full flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                        >
                            <SearchIcon className="flex-shrink-0 h-5 w-5 text-white" />
                        </button>
                    </div>
                </div>

            </form>
        </div>

        {
            status === "success" && <>
                {/* Activity list (smallest breakpoint only) */}
                <div className="shadow mt-5">
                    <ul className="overflow-hidden shadow sm:hidden">
                        {data.filter(filterFunction).map((item: any, index: number) => (
                            <li key={index}>
                                <a onClick={() => handleItemClick(item)} className="block px-4 py-4 bg-white hover:bg-gray-50">
                                    <span className="flex items-center space-x-4">
                                        <span className="flex-1 flex space-x-2 truncate">
                                            <span className="flex flex-col text-gray-500 text-sm truncate">
                                                {
                                                    Object.keys(item).slice(1).slice(0, -1).map((prop, i) => (
                                                        <span className="truncate" key={i}>{item[prop]}</span>
                                                    ))
                                                }
                                            </span>
                                        </span>
                                        <ChevronRightIcon className="flex-shrink-0 h-5 w-5 text-gray-400" aria-hidden="true" />
                                    </span>
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>

                {/* Activity table (small breakpoint and up) */}
                <div className="hidden sm:block mt-8">
                    <div className="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
                        <div className="flex flex-col mt-2">
                            <div className="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
                                <table className="min-w-full divide-y divide-gray-200">
                                    <thead>
                                        <tr>
                                            {
                                                props.column.map((col, i) => (
                                                    <th key={i} className="px-6 py-3 bg-gray-50 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                        {t(col)}
                                                    </th>
                                                ))
                                            }
                                        </tr>
                                    </thead>
                                    <tbody className="bg-white divide-y divide-gray-200">
                                        {data.filter(filterFunction).map((item: any, index: number) => (
                                            <tr onClick={() => { handleItemClick(item) }} key={index} className="bg-white cursor-pointer">
                                                {
                                                    Object.keys(item).slice(1).slice(0, -1).map((prop, i) =>
                                                        <td key={i} className="max-w-0 text-center px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                            {isDateTransformString(item[prop])}
                                                        </td>
                                                    )
                                                }
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        }
    </div>);
}