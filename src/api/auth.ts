import axios, { AxiosResponse } from "axios";
import { API_URL } from ".";

export type API_LOGIN_RESPONSE = string;

export interface API_REGISTER_RESPONSE {
  id: string;
  name: string;
  email: string;
}

export async function login(
  email: string,
  password: string
): Promise<AxiosResponse<API_LOGIN_RESPONSE>> {
  return await axios.post<API_LOGIN_RESPONSE>(API_URL + "/Auth/Login", {
    email,
    password,
  });
}

export async function register(
  name: string,
  email: string,
  password: string
): Promise<AxiosResponse<API_REGISTER_RESPONSE>> {
  return await axios.post<API_REGISTER_RESPONSE>(API_URL + "/Auth/Register", {
    email,
    name,
    password,
  });
}
