import axios from "axios";
import { API_URL } from ".";

export async function ListRescuer(): Promise<any> {
    return (await axios.get<any>(API_URL + "Person/GetPersonThatSavedSomeone")).data;
}

export async function ListRescued(): Promise<any> {
    return (await axios.get<any>(API_URL + "Person/GetPersonThatHaveBeenSaved")).data;
}

export async function ListBoat(): Promise<any> {
    return (await axios.get<any>(API_URL + "Ship/GetShips")).data;
}

export async function GetShipwrecksForSavior(id: string): Promise<any> {
    return (await axios.get<any>(API_URL + `Shipwreck/GetShipwrecksForSavior?savior=${id}`)).data;
}

export async function GetShipwrecksForSavedPeople(id: string): Promise<any> {
    return (await axios.get<any>(API_URL + `Shipwreck/GetShipwrecksForSavedPeople?savedPerson=${id}`)).data;
}

export async function GetShipwrecksForShip(id: string): Promise<any> {
    return (await axios.get<any>(API_URL + `Shipwreck/GetShipwrecksForShip?ship=${id}`)).data;
}