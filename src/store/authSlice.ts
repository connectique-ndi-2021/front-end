import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type AuthState = {
  authenticated: boolean;
  token: string;
  keepConnected: boolean;
};

const initialState: AuthState = {
  authenticated: false,
  token: "",
  keepConnected: false,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loginReducer: (
      state,
      action: PayloadAction<{
        token: string;
        keepConnected: boolean;
      }>
    ) => {
      state.keepConnected = action.payload.keepConnected;

      state.authenticated = true;
      state.token = action.payload.token;

    },
    logout: (state) => {
      state.authenticated = false;
      state.token = "";
    },
  },
});

export const { loginReducer, logout } = authSlice.actions;

export default authSlice.reducer;
